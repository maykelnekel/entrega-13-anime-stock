from flask import request, jsonify
import psycopg2
from app.models.animes_model import Anime


def get_create():
    if request.method == 'GET':
        return jsonify(Anime.get_all()), 200

    elif request.method == 'POST':
        try:
            data = request.get_json()
            keys_list = ["anime", "released_date", "seasons" ]
            for key in data.keys():
                if key not in keys_list:
                    return {"message": "chave invalida"}
                
            anime = Anime(data)
            return anime.create_one(), 201
            

        except psycopg2.errors.UniqueViolation as err:
            return {"message": f'o anime {data["anime"]} já existe'}, 409

        except psycopg2.errors.UndefinedColumn as err:
            correct_keys = ["anime", "released_date", "seasons"]
            data = request.get_json()
            response = {
                "avaiable_keys": correct_keys,
                "wrong_keys_sended": [
                    item for item in data if item not in correct_keys
                ]
                }
            return response, 422


def filter(anime_id):

    animes_list = Anime.get_one(anime_id)

    if len(animes_list["data"]) > 0:
        return animes_list, 200
    else:
        return {"error": "Not found"}, 404
    


def update(anime_id):
    try:
        data = request.get_json()
        anime = Anime.update_one(anime_id, data)
        if len(anime["data"]) > 0:
            return anime, 200
        else:
            return {"error": "Not found"}, 404
    except psycopg2.errors.UndefinedColumn as err:
        correct_keys = ["anime", "released_date", "seasons"]
        response = {
            "avaiable_keys": correct_keys,
            "wrong_keys_sended": [
                item for item in data if item not in correct_keys
            ]
            }
        return response, 422
    


def delete(anime_id):
    return Anime.delete_one(anime_id)
