from flask import Flask
from app import routes
from app.controllers.create_table import create_table

create_table()

def create_app():
    app = Flask(__name__)
    app.config["JSON_SORT_KEYS"] = False
    routes.init_app(app)

    return app
