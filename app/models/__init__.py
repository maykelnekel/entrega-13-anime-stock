from dotenv import load_dotenv
from os import getenv
import psycopg2

load_dotenv()

configs = {
    "host": getenv("host"),
    "database": getenv("database"),
    "user": getenv("user"),
    "password": getenv("password"),
}


def conn_cur():
    conn = psycopg2.connect(**configs)
    cur = conn.cursor()

    return conn, cur


def commit_and_close(conn, cur):
    conn.commit()
    cur.close()
    conn.close()
