
from flask.json import jsonify
from app.controllers.create_table import create_table
from app.models import commit_and_close, conn_cur
from psycopg2 import sql
import pdb

from app.routes import animes


class Anime():

    def __init__(self, values):
        if type(values) is tuple:
            self.id, self.anime, self.released_date, self.seasons = values
        elif type(values) is dict:
            self.anime = values["anime"]
            self.released_date = values["released_date"]
            self.seasons = values["seasons"]

    def create_one(self):
        conn, cur = conn_cur()
        self_anime = self.__dict__

        querry = """
            INSERT INTO
                animes (anime, released_date, seasons)
            VALUES
                (%s, %s, %s)
            RETURNING *;
        """

        querry_values = list(self.__dict__.values())

        cur.execute(querry, querry_values)

        animes_list = cur.fetchall()

        commit_and_close(conn, cur)

        output = [Anime(anime).__dict__ for anime in animes_list]

        return jsonify(output)

    @staticmethod
    def get_all():
        conn, cur = conn_cur()

        create_table()

        querry = """
            SELECT *
            FROM animes;
        """

        cur.execute(querry)

        animes_list = cur.fetchall()

        commit_and_close(conn, cur)

        output = [Anime(anime).__dict__ for anime in animes_list]

        return {"data": output}

    @staticmethod
    def get_one(id):
        conn, cur = conn_cur()

        create_table()

        querry = """
            SELECT *
            FROM animes
            WHERE id = %s;
        """

        cur.execute(querry, str(id))

        animes_list = cur.fetchall()

        commit_and_close(conn, cur)

        output = [Anime(anime).__dict__ for anime in animes_list]

        return {"data": output}

    @staticmethod
    def update_one(anime_id, payload):
        conn, cur = conn_cur()

        columns = [sql.Identifier(key) for key in payload.keys()]
        values = [sql.Literal(value) for value in payload.values()]

        create_table()

        querry = sql.SQL("""
            UPDATE
                animes
            SET
                ({columns}) = row({values})
            WHERE
                id={id}
            RETURNING *;

        """).format(
            id=sql.Literal(anime_id),
            columns=sql.SQL(',').join(columns),
            values=sql.SQL(',').join(values),
        )

        cur.execute(querry, str(anime_id))

        animes_list = cur.fetchall()

        commit_and_close(conn, cur)

        output = [Anime(anime).__dict__ for anime in animes_list]

        return {"data": output}


    @staticmethod
    def delete_one(anime_id):
        conn, cur = conn_cur()

        create_table()

        verify = """
            SELECT *
            FROM animes
            WHERE id = %s;
        """

        cur.execute(verify, str(anime_id))

        animes_list = cur.fetchall()


        if len(animes_list) == 0:
            return {"error": "Not found"}, 404

        else:
            querry = """
                DELETE FROM
                    animes
                WHERE
                    id = %s
                RETURNING *;
            """

            cur.execute(querry, str(anime_id))

            # animes_list = cur.fetchall()

            commit_and_close(conn, cur)

            return '', 204

